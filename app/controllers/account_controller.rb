class AccountController < ApplicationController
  layout 'account'
  layout 'application', :only => [:new, :create]
  before_filter :authenticate, :except => [:new, :create]

  # Show the signup form
  def new
    @user = User.new
  end

  # Create a new user account from the posted signup form
  def create
    @user = User.new params[:user]
    if @user.save
      self.current_user = @user
      redirect_to '/account'
    else
      render :new
    end
  end

  # Show the user's account profile
  def show
  end

  # Show the edit profile form
  def edit
  end

  # Update the user account from the posted edit form
  def update
  end

  # Destroy a user's account
  def destroy
  end 
end
