module ApplicationHelper
  def error_message(model, field)
    return if model.blank?
    return "<div class='error_message'><p>#{model.errors[field].join('</p><p>')}</p></div>".html_safe if model.errors[field].present?
  end
end
