// Regions
mountain
savanna
swamp
forest
jungle
tundra
ocean
sky
cavern
hades

// Aspects
fire       // burning, chaotic, pain, destroying, changing, bright, mesmerizing
earth      // solid, slow, endless, large, coarse, growth
water      // fluid, life-giving, cold, surrounding, cleansing
air        // quick, chi, incorporeal, floating/buoyant
metal      // hard, unbending, brittle, piercing, sharp
energy     // electric, fuel, vibrating, alive, motion, powerful
decay      // death, breaking down, acrid, consuming, fertilizing


        fire      earth      water    air       metal       energy        decay

fire    inferno   glass      steam    explode   forge       power         smoke
earth             bountiful  growth   dust      ore/till    golem         fertilizer
water                        lake     rain      rust        life          mire/ooze
air                                   float     sound       tornado       poison
metal                                           invincible  electricity   shrapnel
energy                                                      excitement    zombie            
decay                                                                     death


// Base Skills
fire     // fireball, sight, mesmerize
earth    // slow, quake, strengthen, defend
water    // heal, ice, shield, cure
air      // fast, float, invisible, escape
metal    // fight, slice, counter
energy   // lightning, energize, 
decay    // weaken, confuse, expel

// Subtractive -- left hand side always defeats or reduces the right hand side
fire   --> decay
earth  --> air
water  --> fire
air    --> metal
metal  --> energy
energy --> earth
decay  --> water

// Additive -- these combos always benefit each other
fire   <++> air
earth  <++> water
metal  <++> energy
decay  <++> decay 


///////////////////////////////////////////
// Dungeons
///////////////////////////////////////////
Dungeons
{
  owner: "ooog",
  value: 1000,
  rooms: [
    {
      
    },
  ]
}

///////////////////////////////////////////
// Hero and Monster Base Templates
///////////////////////////////////////////
CreatureTemplates {
  name: "machinist",
  level: 1,
  hp: 10,

  // User can assign equipment to these slots
  weapon: "",      // wea
  guard: "",       // gua
  neck: "",        // nec
  arm: "",         // arm
  
  skills: [        // ski
    "attack", 
    "defend", 
    "fireball", 
    "sneak", 
    ""],
   
  // Stats
  strength: 5,     // str
  speed: 5,        // spe
  focus: 5,        // foc
  intelligence: 5, // int
  luck: 5,         // luc
  vitality: 5,     // vit
  attack: 5,       // att
  parry: 5,        // par
  
  // Pre-calculated stats
  damage: 5,       // dam
  defend: 5,       // def
}

///////////////////////////////////////////
// Items, Weapons, Armor, etc.
///////////////////////////////////////////
Items
{
  name: "",
  description: "",
  frequency: 5,     // 
  aspect: "fire",
  region: "mountain",
  effects: {
    attack: +1,
    luck: -2
  }
}


///////////////////////////////////////////
// Skills
///////////////////////////////////////////
Skills
{
  name: "attack",
  battle: 0,          // 0 == can be used outside of a fight, 1 == only in a fight, 2 == can be used anywhere
  multi: 0,           // 0 == only target one, 1 == targets all in group
  aspect: "metal",
  effect: {
    target: { hp: function(c, t) { t.hp -= c.damage; } },
    caster: {  }
  }
},
{
  name: "heal",
  battle: 2,
  aspect: "water",
  effect: {
    target: { hp: +10 },
    caster: { int: -1 }
  }
},
{
  name: "fireball",
}
