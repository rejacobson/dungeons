require 'spec_helper'

describe AccountController, '#new' do

  describe "GET 'new'" do
    it "returns http success" do
      get :new
      response.should be_success
    end

    it "should create a new user" do
      User.should_receive :new
      get :new
    end
  end

end

describe AccountController, '#create' do
  before(:each) do
    @valid_params = {'username' => 'ooog', 'email' => 'asdf@example.com', 'password' => '123456' , 'password_confirmation' => '123456'} 
    @user = User.new @valid_params
  end

  it "should render 'signup' if no params are passed" do
    post :create
    response.should render_template(:new)
  end

  it "uses passed in params to create a user" do
    User.should_receive(:new).with(@valid_params).and_return @user
    post :create, :user => @valid_params
  end

  it "should save the user" do
    User.should_receive(:new).with(@valid_params).and_return @user
    controller.should_receive(:current_user=).with @user
    post :create, :user => @valid_params
    response.should redirect_to('/account')
  end

end

describe AccountController, '#show' do
  it 'requires a logged in user' do
    controller.should_receive(:authenticate)
    get :show
  end

  it 'returns http success' do
    controller.stub(:authenticate)
    get :show
    response.should be_success
  end
end
