require 'spec_helper'

describe ApplicationHelper, '#error_message' do
  before(:each) do
    @user = User.new
  end

  it "should return a div with an error message" do
    @user.save
    error_message(@user, :password).should_not be_nil
  end

  it "should return nil when no errors are present" do
    @user.stub(:errors).and_return({})
    error_message(@user, :password).should be_nil 
  end
end 
