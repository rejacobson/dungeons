class SessionController < ApplicationController
  # Show the login form
  def new
    @user = User.new
  end

  # Log the user in
  def create
    @user = User.authenticate(params[:email], params[:password])
    render :new and return if @user.nil?
    self.current_user = @user
    redirect_to account_path
  end

  # Log the user out
  def destroy
    sign_out_keeping_session!
    redirect_to root_path
  end
end
