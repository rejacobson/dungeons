require 'spec_helper'

describe SessionController, '#new' do
  it "returns http success" do
    get :new
    response.should be_success
  end

  it "should create a new user" do
    User.should_receive :new
    get :new
  end
end

describe SessionController, '#create' do
  before(:each) do
    @params = { 'email' => 'rejacobson@gmail.com', 'password' => '123456', 'username' => 'myname' }
    @user = User.new @params
    @user.save
  end

  it 'authenticates a user with the :email and :password' do
    User.should_receive(:authenticate).with(@params['email'], @params['password']).and_return @user
    controller.should_receive(:current_user=).with @user
    post :create, @params
  end

  it 'redirects to the account page' do
    User.stub(:authenticate).and_return @user
    post :create, @params
    response.should redirect_to account_path
  end

  it 'should render the login page if user validation failed' do
    User.stub(:authenticate).and_return nil
    post :create, @params
    response.should render_template(:new)
  end

end

describe SessionController, '#destroy' do
  it 'logs the user out' do
    controller.should_receive(:sign_out_keeping_session!)
    delete :destroy
  end

  it 'redirects the user to the home page' do
    delete :destroy
    response.should redirect_to(root_path)
  end
end
